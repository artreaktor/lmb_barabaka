CLEAN INSTALL:
1. Configure DB connection in web/sites/default/settings.php
2. Import /backups/1.sql into yur database
3. Enable Lemberg Barabaka module
4. Run Cron to fetch data from giphy.com
5. Configure webform and panels manually

FULLY CONFIGURED:
1. Configure DB connection in web/sites/default/settings.php
2. Import /backups/2.sql into yur database
3. Run Cron to fetch data from giphy.com

If your cron doesn't trigger content parsing check cron settings here:
admin/config/barabaka

See also web/sites/all/modules/custom/lemberg_barabaka/README.txt