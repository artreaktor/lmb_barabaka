<?php

/**
 * @file
 * Provides barabakas panels pane.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Latest Barabakas'),
  'description' => t('Displays latest Barabakas'),
  'category' => t('Custom Panes'),
  'edit form' => 'lemberg_barabaka_latest_barabakas_form',
  'render callback' => 'lemberg_barabaka_latest_barabakas_render',
  'admin info' => 'lemberg_barabaka_latest_barabakas_info',
  'defaults' => array(),
  'all contexts' => TRUE,
);

/**
 * Pane configuration form.
 */
function lemberg_barabaka_latest_barabakas_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['quantity'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of barabakas'),
    '#description' => t('How many barabakas you want?'),
    '#default_value' => $conf['quantity'],
  );

  return $form;
}

/**
 * Pane configuration form validation handler.
 */
function lemberg_barabaka_latest_barabakas_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['quantity'])) {
    form_set_error('quantity', t('Only numbers allowed!'));
  }
}

/**
 * Pane configuration form submit handler.
 */
function lemberg_barabaka_latest_barabakas_form_submit($form, &$form_state) {
  $form_state['conf']['quantity'] = $form_state['values']['quantity'];
}

/**
 * Render the pane.
 */
function lemberg_barabaka_latest_barabakas_render($subtype, $conf, $args, $contexts) {
  // Render as a block.
  $block = new stdClass();
  $block->module = 'entity';
  $block->delta = 'latest-barabakas-' . $conf['quantity'];

  // Load view and set items per page
  $view = views_get_view('latest_barabakas');
  $view->set_display('block');
  $view->set_items_per_page($conf['quantity']);
  $view->execute();
  $block->content = $view->preview();

  return $block;
}

/**
 * Admin info.
 */
function lemberg_barabaka_latest_barabakas_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $content = '<p><b>Barabakas displayed:</b> ' . $conf['quantity'] . '</p>';

    $block = new stdClass;
    $block->title = $conf['override_title'] ? $conf['override_title_text'] : '';
    $block->content = $content;
    return $block;
  }
}
