<?php

class BarabakaWebservicesController extends WebservicesController {

  protected $entityBundle;

  public function __construct($entity_type, $entity_bundle, $args = array()) {
    $this->entityBundle = $entity_bundle;
    parent::__construct($entity_type, $args);
  }

  protected function getEntityController($args = array()) {
    $controller = webservices_get_controller($this->entityBundle);
    if ($controller) {
      return new $controller($this->requestMethod, $this->requestData, $args);
    }
    else {
      return FALSE;
    }
  }
}
