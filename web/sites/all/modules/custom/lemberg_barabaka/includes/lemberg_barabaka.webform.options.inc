<?php

/**
 * Webform options provider.
 */
function lemberg_barabaka_webform_options($component, $flat, $arguments) {

  $barabakas = db_select('node')
    ->fields('node', array('nid', 'title'))
    ->condition('type', 'barabaka')
    ->execute()
    ->fetchAllKeyed(0, 1);

  return $barabakas;
}