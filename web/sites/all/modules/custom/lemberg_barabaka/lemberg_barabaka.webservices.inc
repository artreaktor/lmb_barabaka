<?php

/**
 * Implements hook_webservices_controller.
 */
function lemberg_barabaka_webservices_controller() {
  return array(
    'barabaka' => 'WebservicesBarabakaController'
  );
}
