Lemberg Barabaka module provides:
1. Giphy.com cron-controlled parser/importer (using queue API)
2. Custom content type
3. Webform integration
4. REST integration with ​https://drupal.org/sandbox/Taran2L/1807378
5. Panels integration

Configuration page: admin/config/barabaka